﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApis.App_Start;
using WebApis.Models;


namespace WebApis.Controllers
{
    
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        //  MongoContext _dbContext;

        public UserController ()
        {
            // _dbContext = new MongoContext();
        }

        [HttpGet]
        [Route("getusers")]
        public object GetUsers () {
            string constr = ConfigurationManager.AppSettings["connectionString"];
            var Client = new MongoClient(constr);
            var DB = Client.GetDatabase("user");
            var collection = DB.GetCollection<UserModel>("user").Find(new BsonDocument()).ToList();
            return collection;
        }

   
        [HttpGet]
        [Route("saveusers")]
        public object SaveUsers (string name, string email, string phone)
        {
            UserModel userModel = new UserModel {
                Name = name,
                Email = email,
                Phone = phone
            };
            string constr = ConfigurationManager.AppSettings["connectionString"];
            var Client = new MongoClient(constr);
            var DB = Client.GetDatabase("user");
            var collection = DB.GetCollection<UserModel>("user").InsertOneAsync(userModel);
            return "OK";
        }

        [HttpPut]
        [Route("edituser")]
        public object UpdateUser ([FromBody] UserModel userModel) {
            string constr = ConfigurationManager.AppSettings["connectionString"];
            var Client = new MongoClient(constr);
            var DB = Client.GetDatabase("user");
            var collection = DB.GetCollection<UserModel>("user");
            var update = collection.FindOneAndUpdateAsync(Builders<UserModel>.Filter.Eq("email", userModel.Email), 
                Builders<UserModel>.Update.Set("name", userModel.Name)
                .Set("phone",userModel.Phone)
                .Set("family",userModel.Family));
            return "Ok";
        }

        public object DeleteUser (string email) {
            string constr = ConfigurationManager.AppSettings["connectionString"];
            var Client = new MongoClient(constr);
            var DB = Client.GetDatabase("user");
            var collection = DB.GetCollection<UserModel>("user");
            collection.FindOneAndDelete(Builders<UserModel>.Filter.Eq("email", email));
            return "ok";
        }

    }
}
