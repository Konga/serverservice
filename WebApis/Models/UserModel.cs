﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WebApis.Models
{
    public class UserModel
    {
        [BsonId]
        public ObjectId Id { get; set; }
        [BsonElement("name")]
        public String Name { get; set; }
        [BsonElement("email")]
        public String Email { get; set; }
        [BsonElement("password")]
        public String Password { get; set; }
        [BsonElement("phone")]
        public String Phone { get; set; }
        [BsonElement("family")]
        public List<Family> Family { get; set; }
    }

    public class Family
    {
        [BsonElement("name")]
        public String Name { get; set; }
        [BsonElement("relation")]
        public String Relation { get; set; }
    }
}